//
// Debtags infrastructure
//

class Tagset
{
    constructor()
    {
        this._items = {};
        this._size = 0;
        for (var i = 0; i < arguments.length; ++i)
            this.add(arguments[i]);
    }

    clear()
    {
        this._items = {};
        this._size = 0;
    }

    has(name)
    {
        return this._items.hasOwnProperty(name);
    }

    hasRE(re)
    {
        var res = false;
        this.each(function(t) { if (re.exec(t)) res = true; });
        return res;
    }

    add(name)
    {
        if (this.has(name))
            return false;
        this._items[name] = true;
        ++this._size;
        return true;
    }

    add_all(names)
    {
        for (let i in names)
            this.add(names[i]);
    }

    del(name)
    {
        if (this.has(name))
        {
            delete this._items[name];
            --this._size;
            return true;
        }
        return false;
    }

    patch(added, removed)
    {
        for (const t of added)
            this.add(t);
        for (const t of removed)
            this.del(t);
    }

    size()
    {
        return this._size;
    }

    empty()
    {
        return this._size == 0;
    }

    addFirstTags(tags, coll)
    {
        var lastCard = 0;
        function score(x) { return ((x-15)*(x-15))/x; }

        for (i in tags) {
            var tag = tags[i][0];
            var card = parseInt(tags[i][1]);
            if (i == 0 || score(lastCard) > score(card)) {
                this.add(tag);
                lastCard = card;
            } else
                break;
        }

        // Return always at least the first tag
        if (this.empty() && tags.length > 0)
            this.add(tags[0][0]);
    }

    each(fun)
    {
        for (var i in this._items)
            fun(i);
    }

    merge(tset)
    {
        var tmp=this;
        tset.each(function(i){tmp.add(i)});
    }

    subtract(tset)
    {
        var tmp=this;
        tset.each(function(i){tmp.del(i)});
    }

    contains(tset)
    {
        for (var i in tset._items)
            if (!this.has(i))
                return false;
        return true;
    }

    intersects(tset)
    {
        for (var i in tset._items)
            if (this.has(i))
                return true;
        return false;
    }

    to_list()
    {
        var res = [];
        for (var i in this._items)
            res.push(i);
        return res;
    }

    toString()
    {
        var str = "";
        this.each(function(i){if(str == "") str+=i; else str+=', '+i;});
        return str;
    }

    copy()
    {
        var res = new Tagset();
        this.each(function(i){res.add(i);});
        return res;
    }

    mkpatch(ts)
    {
        var added = ts.copy();
        added.subtract(this);
        var removed = this.copy();
        removed.subtract(ts);
        var res = new Tagset();
        added.each(function(tag){res.add("+"+tag);});
        removed.each(function(tag){res.add("-"+tag);});
        return res;
    }
}


class Vocabulary
{
    constructor(fdata, tdata)
    {
        this.facs = fdata;
        this.tags = tdata;
        this.ftags = {};
        for (var i in this.tags) {
            var tmp = i.split("::");
            if (! this.ftags.hasOwnProperty(tmp[0]))
                this.ftags[tmp[0]] = new Tagset();
            this.ftags[tmp[0]].add(i);
        }
    }

    // Return an array [shortdesc, longdesc]
    facData(fac)
    {
        let res = this.facs[fac];
        if (res == null) res = [fac, fac];
        return res;
    }

    // Return an array [shortdesc, longdesc]
    tagData(tag)
    {
        let res = this.tags[tag];
        if (res == null) res = [tag, tag];
        return res
    }

    facetOfTag(tag)
    {
        return tag.split("::")[0];
    }

    tagsOfFacet(fac)
    {
        return this.ftags[fac];
    }

    eachFacet(fun)
    {
        var facets = [];
        for (var i in this.facs)
            facets.push(i);
        facets.sort();

        for (var i in facets)
        {
            var facet = this.facs[facets[i]];
            fun(facets[i], facet[0], facet[1]);
        }
    }

    eachTag(fun)
    {
        for (var i in this.tags) {
            fun(i, this.tags[i][0], this.tags[i][1]);
        }
    }

    eachTagOfFacet(fac, fun)
    {
        var ts = this.tagsOfFacet(fac);
        var tags = [];
        if (ts) ts.each(function(t) { tags.push(t); });
        tags.sort();

        var tmp = this;
        for (var i in tags)
        {
            var tag = tags[i];
            fun(tag, tmp.tags[tag][0], tmp.tags[tag][1]);
        }
    }
}


class Patch
{
    constructor()
    {
        this.added = {};
        this.removed = {};
    }

    clear()
    {
        this.added = {};
        this.removed = {};
    }

    add(tag)
    {
        this.added[tag] = true;
        delete this.removed[tag];
    }

    del(tag)
    {
        this.removed[tag] = true;
        delete this.added[tag];
    }

    update(added, removed)
    {
        // Scan for items common to both sets
        var seen = {};
        var common = {};
        for (const tag of added) { seen[tag] = true; }
        for (const tag of removed) { if (seen[tag]) common[tag] = true; }

        // Perform changes, taking note of what actually changed
        var touched = [];
        for (const t of added)
        {
            if (common[t]) continue;
            let changed = false;
            if (!this.added[t])
            {
                this.added[t] = true;
                changed = true;
            }
            if (this.removed[t])
            {
                delete this.removed[t];
                changed = true;
            }
            if (changed)
                touched.push(t);
        }
        for (const t of removed)
        {
            if (common[t]) continue;
            let changed = false;
            if (!this.removed[t])
            {
                this.removed[t] = true;
                changed = true;
            }
            if (this.added[t])
            {
                delete this.added[t];
                changed = true;
            }
            if (changed)
                touched.push(t);
        }
        return touched;
    }

    reset(tag)
    {
        if (this.added[tag] || this.removed[tag])
        {
            delete this.added[tag];
            delete this.removed[tag];
            return true;
        } else
            return false;
    }

    has(tag)
    {
        return this.added[tag] || this.removed[tag];
    }

    get(tag)
    {
        if (this.added[tag]) return "+";
        if (this.removed[tag]) return "-";
        return null;
    }

    sorted()
    {
        var res = [];
        for (var i in this.added)
            res.push("+" + i);
        for (var i in this.removed)
            res.push("-" + i);
        res.sort();
        return res;
    }

    each(f)
    {
        var all = this.sorted();
        for (var i in all)
        {
            f(all[i]);
        }
    }

    to_string()
    {
        return this.sorted().join(", ");
    }

    is_empty()
    {
        for (var i in this.added)
            return false;
        for (var i in this.removed)
            return false;
        return true;
    }

    is_added(tag)
    {
        return this.added[tag] == true;
    }

    is_removed(tag)
    {
        return this.removed[tag] == true;
    }

    count_changes()
    {
        var res = 0;
        for (var i in this.added)
            ++res;
        for (var i in this.removed)
            ++res;
        return res;
    }

    all_tags()
    {
        var res = [];
        for (var i in this.added)
            res.push(i);
        for (var i in this.removed)
            res.push(i);
        return res;
    }

    /**
     * Returns true if patch is a subset of this patch
     */
    contains(patch)
    {
        var self = this;
        for (var t in patch.added)
            if (!self.added[t])
                return false;
        for (var t in patch.removed)
            if (!self.removed[t])
                return false;
        return true;
    }

    /**
     * Returns true if patch conflicts with this patch
     */
    conflicts(patch)
        {
        var self = this;
        for (var t in patch.added)
            if (self.removed[t])
                return true;
        for (var t in patch.removed)
            if (self.added[t])
                return true;
        return false;
    }

    simplify(tagset)
    {
        for (const tag in this.added)
            if (tagset.has(tag))
                delete this.added[tag];

        for (const tag in this.removed)
            if (!tagset.has(tag))
                delete this.removed[tag];
    }
}


class Check
{
    constructor(type, msg)
    {
        this.type = type;
        this.msg = msg;
        this.fixes = [];
    }

    add_fix(msg, patch)
    {
        this.fixes.push({ msg: msg, patch: patch });
    }
}


function facet_of_tag(tag)
{
    return tag.split("::")[0];
}

function ajax(options)
{
    return new Promise((resolve, reject) => {
        let args = $.extend({}, options, {
            success: (data, textStatus, jqXHR) => {
                console.debug("API AJAX OK", args, "→", data);
                resolve(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.warn("API AJAX ERROR", args, "→", jqXHR, textStatus, errorThrown);
                reject(errorThrown);
            },
        });
        $.ajax(args);
    });
}

