#!/usr/bin/python

# run-apriori: run apriori algorithm on debtags DB
#
# Copyright (C) 2007--2011  Enrico Zini <enrico@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from debdata.apriori import Apriori
from debdata import utils
from debian import debtags

if __name__ == "__main__":
    from optparse import OptionParser
    import sys
    import logging

    log = logging.getLogger(sys.argv[0])

    VERSION="0.1"

    class Parser(OptionParser):
        def error(self, msg):
            sys.stderr.write("%s: error: %s\n\n" % (self.get_prog_name(), msg))
            self.print_help(sys.stderr)
            sys.exit(2)

    parser = Parser(usage="usage: %prog [options]",
                    version="%prog "+ VERSION,
                    description="Compute tag correlation statistics using Christian Borgelt's apriori implementation")
    parser.add_option("-i", "--input", action="store", metavar="file", default="/var/lib/debtags/package-tags",
                      help="input file with the debtags tag database (default: %default)")
    parser.add_option("--impl", action="store", metavar="executable", default="./apriori",
                      help="executable of Christian Borgelt's apriori implementation (default: %default)")
    parser.add_option("-q", "--quiet", action="store_true", help="quiet mode: only output warnings and errors")
    parser.add_option("-r", "--reverse", action="store_true", help="reverse mode: compute negative associations")
    #parser.add_option("-v", "--verbose", action="store_true", help="verbose mode")
    #parser.add_option("-f", "--force", action="store_true", help="force database rebuild even if it's already up to date")
    (opts, args) = parser.parse_args()

    if opts.quiet:
        logging.basicConfig(level=logging.WARNING, stream=sys.stderr)
    else:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr)

    log.info("Reading debtags data from %s...", opts.input)
    db = Apriori.read_debtags_db(opts.input)

    # Compute facet collection (unfortunately db.facet_collection is currently broken)
    #fdb = db.facet_collection()
    fdb = debtags.DB()
    for pkg, tags in db.iter_packages_tags():
        tags = set(utils.tags_to_facets(tags))
        fdb.insert(pkg, tags)

    # Configure the computation engine
    apriori = Apriori(quiet=opts.quiet, reverse=opts.reverse)
    apriori.conf_apriori = opts.impl

    def print_rule(type, a):
        print "%s %s %f %f: %s" % (type, a.tgt, a.sus, a.conf, ", ".join(sorted(a.src)))

    log.info("Computing facet correlations...")
    for a in apriori.run_apriori(fdb):
        print_rule("f", a)

    log.info("Computing tag correlations...")
    for a in apriori.run_apriori(db):
        print_rule("t", a)

