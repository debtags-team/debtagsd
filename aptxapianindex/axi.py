# coding: utf8
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.conf import settings
import xapian
import os.path
from backend.models import Package


class AptXapianIndex(object):
    def __init__(self):
        self.pathname = getattr(settings, "BACKEND_AXI", None)
        if self.pathname is None:
            self.pathname = os.path.join(getattr(settings, "BACKEND_DATADIR", "data"), "axi", "index")
        self.db = xapian.Database(self.pathname)
        self.stem = xapian.Stem("english")

    def query(self, text, wildcard=False):
        qp = xapian.QueryParser()
        qp.set_default_op(xapian.Query.OP_AND)
        qp.set_database(self.db)
        qp.set_stemmer(self.stem)
        qp.set_stemming_strategy(xapian.QueryParser.STEM_SOME)
        qp.add_prefix("pkg", b"XP")
        qp.add_boolean_prefix("tag", b"XT")
        qp.add_boolean_prefix("sec", b"XS")

        flags = \
            xapian.QueryParser.FLAG_BOOLEAN | \
            xapian.QueryParser.FLAG_LOVEHATE | \
            xapian.QueryParser.FLAG_BOOLEAN_ANY_CASE | \
            xapian.QueryParser.FLAG_PURE_NOT | \
            xapian.QueryParser.FLAG_SPELLING_CORRECTION | \
            xapian.QueryParser.FLAG_AUTO_SYNONYMS

        if wildcard:
            flags |= xapian.QueryParser.FLAG_WILDCARD

        if not text:
            return None, qp
        else:
            return qp.parse_query(text, flags), qp

    def get_docid(self, pkg):
        """
        Get the Xapian document ID for a package name
        """
        res = None
        for p in self.db.postlist('XP'+str(pkg)):
            res = p.docid
        return res

    def packages_keyword_search(self, query, first=0, count=50):
        """
        Full-text search on packages
        """
        enq = xapian.Enquire(self.db)
        q, qp = self.query(query)
        enq.set_query(q)
        percs = dict()
        res = []
        for m in enq.get_mset(first, count):
            percs[m.document.get_data()] = m.percent
            res.append(m.document.get_data())
        for pkg in Package.by_names(res):
            yield dict(perc=percs[pkg.name], pkg=pkg)

    def suggest_tags(self, pkg, count=30):
        """
        Suggest tags for the given package
        """
        # Build a query with all the terms from this package
        docid = self.get_docid(pkg.name.encode("utf8"))
        if docid is None:
            return
        terms = [x.term for x in self.db.termlist(docid)]
        q = xapian.Query(xapian.Query.OP_ELITE_SET, terms)

        enq = xapian.Enquire(self.db)
        enq.set_query(q)

        # Use the top 20 results as an Rset
        rset = xapian.RSet()
        for m in enq.get_mset(0, 20):
            rset.add_document(m.docid)

        # Filter out non-tag terms, and special tags
        class TagFilter(xapian.ExpandDecider):
            def __call__(self, term):
                return term[:2] == b"XT" and not term.startswith(b"XTspecial::")

        # Produce results
        # First the package's own tags
        seen = set()
        for t in terms:
            if t.startswith(b"XT"):
                tag = t[2:].decode("utf8")
                seen.add(tag)
                yield tag
        # The the suggested ones
        for e in enq.get_eset(count, rset, TagFilter()):
            tag = e.term[2:].decode("utf8")
            if tag not in seen:
                yield tag

    def search_tags(self, query, count=30):
        """
        Search tags through Apt Xapian index.

        See http://www.enricozini.org/2007/debtags/axi-query-tags/
        """
        enq = xapian.Enquire(self.db)

        # Query documents
        q, qp = self.query(query)
        enq.set_query(q)

        # Use the top 20 as an Rset
        rset = xapian.RSet()
        for m in enq.get_mset(0, 20):
            rset.add_document(m.docid)

        # Filter out non-tags, and special tags
        class TagFilter(xapian.ExpandDecider):
            def __call__(self, term):
                return term[:2] == b"XT" and not term.startswith(b"XTspecial::")

        # Produce results
        for e in enq.get_eset(count, rset, TagFilter()):
            yield e.term[2:].decode("utf8"), e.weight
