from __future__ import annotations
from django import http, forms
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
from django.views.generic import View
from django.conf import settings
import json
from io import StringIO
from debtagslayout.mixins import DebtagsLayoutMixin
import backend.models as bmodels
from aptxapianindex.axi import AptXapianIndex
from backend.utils import add_cors_headers
from debdata import patches
import calendar
import logging

log = logging.getLogger(__name__)


def serialize_default(obj):
    tt = getattr(obj, "timetuple", None)
    if tt is not None:
        return calendar.timegm(tt())
    else:
        raise TypeError('Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj)))


def serialize_to_response(request, data):
    response = http.HttpResponse(content_type="application/json")
    add_cors_headers(response)
    json.dump(data, response, default=serialize_default)
    return response


class PatchForm(forms.Form):
    patch = forms.CharField(widget=forms.Textarea())


class SubmitPatch(DebtagsLayoutMixin, FormView):
    form_class = PatchForm
    template_name = "api/post.html"

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            raise PermissionDenied
        return super().dispatch(request, *args, **kw)

    def form_valid(self, form):
        """
        Submit a tag patch
        """
        # Parse and validate the patch
        patchset = patches.PatchSet(fd=StringIO(form.cleaned_data["patch"]))

        # Apply the patch, enforcing constraints from anonymous submissions
        db = bmodels.Patches()
        pkgs, checker = db.apply_user_patchset(self.request.user, patchset)

        response = http.HttpResponse(content_type="application/json")
        add_cors_headers(response)
        response.write(json.dumps(dict(
            pkgs=dict(((pkg.name, [x.name for x in pkg.stable_tags.all()]) for pkg in pkgs)),
            notes=checker.remarks,
        )))
        return response


def tag_search_view(request, query):
    """
    Search tags by keyword
    """
    res = []
    if query:
        axi = AptXapianIndex()
        for tag, weight in axi.search_tags(query):
            res.append(dict(n=tag, w=weight))

    response = http.HttpResponse(content_type="application/json")
    add_cors_headers(response)
    response.write(json.dumps(dict(tags=res)))
    return response


class GitlabVocabularyPipeline(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kw):
        return super().dispatch(request, *args, **kw)

    def post(self, request, *args, **kw):
        gitlab_pipeline_name = getattr(settings, "VOCABULARY_GITLAB_PIPELINE_NAME", "tests")
        gitlab_token = getattr(settings, "VOCABULARY_GITLAB_TOKEN", None)
        if gitlab_token is not None:
            if gitlab_token != request.META.get("HTTP_X_GITLAB_TOKEN"):
                log.error("Invalid gitlab token")
                raise PermissionDenied

        log.info("gitlab submission %s", request.body.decode("utf8"))

        data = json.loads(request.body.decode("utf8"))

        if data.get("object_kind") != "pipeline":
            log.error("Wrong object_kind %s", data.get("object_kind"))
            raise PermissionDenied

        builds = data.get("builds")
        if not isinstance(builds, list):
            log.error("Wrong builds type")
            raise PermissionDenied

        builds = [b for b in builds if b.get("name") == gitlab_pipeline_name]
        if not builds:
            log.error("Found %s build not found", gitlab_pipeline_name)
            raise PermissionDenied

        if builds[0].get("status") != "success":
            log.error("Wrong build status: %s", builds[0].get("status"))
            raise PermissionDenied

        attrs = data.get("object_attributes")
        if not attrs and not isinstance(attrs, dict):
            log.error("Wrong object_attributes type: %r", attrs)
            raise PermissionDenied

        log.info("updating vocabulary with git commit %s", attrs.get("sha"))
        bmodels.fetch_new_vocabulary(attrs.get("sha"))
        bmodels.load_vocabulary()
        log.info("vocabulary updated")

        return http.HttpResponse("", content_type="text/plain")
