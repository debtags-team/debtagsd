from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import BackendDataMixin
from unittest import mock


@override_settings(BACKEND_DATADIR="testdata")
class TestView(BackendDataMixin, TestCase):
    def test_index(self):
        client = self.make_test_client()
        response = client.get(reverse("api_index"))
        self.assertContains(response, "This page document the site JSON APIs")

    def test_tag_search(self):
        client = self.make_test_client()
        response = client.get(reverse("api_tag_search", args=["package"]))
        self.assertEquals(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/json")
        data = response.json()
        tags = data["tags"]
        if not {"suite::debian", "admin::package-management"}.issubset({x["n"] for x in tags}):
            self.fail(f"{tags!r} does not contain at least suite::debian and admin::package-management")

    def test_patch(self):
        client = self.make_test_client()
        response = client.get(reverse("api_patch"))
        self.assertEqual(response.status_code, 403)

        response = client.post(reverse("api_patch"), data={"patch": "debtags: +x11::font"})
        self.assertEqual(response.status_code, 403)

        client = self.make_test_client(self.user)
        response = client.get(reverse("api_patch"))
        self.assertContains(response, "This page allows to post a tag patch manually")

        response = client.post(reverse("api_patch"), data={"patch": "debtags: +x11::font"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/json")
        res = response.json()
        self.assertEqual(res["notes"], [])
        patch = res["pkgs"].pop("debtags")
        self.assertFalse(res["pkgs"])
        self.assertCountEqual(
            patch, [
                'suite::debian', 'x11::font', 'interface::commandline',
                'use::searching', 'implemented-in::c++', 'role::program',
                'scope::utility'])

        response = client.post(reverse("api_patch"), data={"patch": "debtags: +special::not-yet-tagged"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/json")
        res = response.json()
        self.assertEqual(res["notes"], ["debtags: ignoring attempt to add special::* tags"])
        self.assertFalse(res["pkgs"])

    def test_gitlab_vocabulary_pipeline(self):
        client = self.make_test_client()
        url = reverse("api_gitlab_vocabulary_pipeline_hook")
        with mock.patch("backend.models.fetch_new_vocabulary") as bfetch:
            with mock.patch("backend.models.load_vocabulary") as bload:
                with self.assertLogs():
                    response = client.post(url, content_type="application/json", data={
                        "object_kind": "pipeline",
                        "builds": [
                            {"name": "tests", "status": "success"},
                        ],
                        "object_attributes": {
                            "sha": "12345678",
                        },
                    })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "text/plain")
        self.assertEqual(response.content, b"")
        bfetch.assert_called_with("12345678")
        bload.assert_called_with()
