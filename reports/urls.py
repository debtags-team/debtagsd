from __future__ import annotations
from django.conf.urls import url
from debtagslayout.views import DebtagsTemplateView
from . import views

urlpatterns = [
    url(r'^$', DebtagsTemplateView.as_view(template_name='reports/index.html'), name="report_index"),
    url(r'^stats/$', views.Stats.as_view(), name="report_stats"),
    url(r'^getting-started/$', DebtagsTemplateView.as_view(template_name="reports/getting-started.html"),
        name="report_getting_started"),
    url(r'^todo/$', views.TodoView.as_view(), name="report_todo"),
    url(r'^todo/maint/(?P<email>[^/]+)$', views.TodoMaintView.as_view(), name="report_maint"),
    url(r'^recent/$', views.Recent.as_view(), name="report_recent"),
    url(r'^maint/$', DebtagsTemplateView.as_view(template_name='reports/maint-index.html'), name="report_maint_index"),
    url(r'^maint/(?P<email>[^/]+)$', views.TodoMaintView.as_view(), name="report_todo_maint"),
    url(r'^checks/$', views.ListChecks.as_view(), name="report_checks_list"),
    url(r'^checks/(?P<id>\d+)$', views.ShowCheck.as_view(), name="report_checks_show"),
    url(r'^pkginfo/(?P<name>[^/]+)$', views.PkgInfo.as_view(), name="report_pkginfo"),

    url(r'^facets$', views.Facets.as_view(), name="report_facets"),
    url(r'^facinfo/(?P<name>[^/]+)$', views.Facet.as_view()),  # Compatibility
    url(r'^facets/(?P<name>[^/]+)$', views.Facet.as_view(), name="report_facet"),

    url(r'^taginfo/(?P<name>[^/]+)$', views.TagInfo.as_view(), name="report_tag"),
    url(r'^history$', views.AuditLog.as_view(), name="report_audit_log"),
    url(r'^apriori-rules$', views.AprioriRules.as_view(), name="report_apriori_rules"),

    url(r'^tags_for_maintainer$', views.TagsForMaintainer.as_view(), name="report_tags_for_maintainer"),
]
