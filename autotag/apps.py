from __future__ import unicode_literals

from django.apps import AppConfig


class AutotagConfig(AppConfig):
    name = 'autotag'
