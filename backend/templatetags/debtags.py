from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(needs_autoescape=True)
def tag(value, autoescape=True):
    if autoescape:
        value = conditional_escape(value)
    if value.startswith("special::"):
        pill = "badge-warning"
    elif value.startswith("role::"):
        pill = "badge-success"
    else:
        pill = "badge-info"
    return mark_safe('<span class="badge badge-pill {}">{}</span>'.format(pill, value))
