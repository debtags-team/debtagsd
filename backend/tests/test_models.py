from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division
from django.test import TestCase, override_settings
from backend.unittest import BackendDataMixin
from debian import debtags
import backend.models as bmodels
import os
import os.path
import shutil

@override_settings(BACKEND_DATADIR="testdata")
class TestBackend(BackendDataMixin, TestCase):
    def test_package(self):
        "Test Package core functions"
        p = bmodels.Package.objects.get(name="debtags")

        # Test by_name
        p1 = bmodels.Package.by_name("debtags")
        self.assertEqual(p, p1)
        self.assertIsNone(bmodels.Package.by_name("this-package-does-not-exist"))

        # Test comparison with deferreds
        dp = bmodels.Package.objects.filter(name="debtags").only("name")[0]
        self.assertEqual(p, dp)

        # Test members
        self.assertEqual(p.source, "debtags")
        self.assertIn("tag", p.sdesc)
        self.assertIn("Debian", p.ldesc)
        self.assertGreater(p.popcon, 0)
        self.assertIn("implemented-in::c++", [t.name for t in p.stable_tags.all()])

        d = p.to_dict()
        self.assertIsInstance(d, dict)
        self.assertIn("name", d)
        self.assertIn("sdesc", d)
        self.assertIn("popcon", d)
        self.assertIn("source", d)
        self.assertIn("ldesc", d)
        self.assertIsInstance(d["stable_tags"], list)
        self.assertIsInstance(d["todolist"], list)

        d = p.to_dict(with_ldesc=False)
        self.assertIsInstance(d, dict)
        self.assertIn("name", d)
        self.assertIn("sdesc", d)
        self.assertIn("popcon", d)
        self.assertIn("source", d)
        self.assertNotIn("ldesc", d)
        self.assertIsInstance(d["stable_tags"], list)
        self.assertIsInstance(d["todolist"], list)

        d = p.to_dict(with_stags=False)
        self.assertIsInstance(d, dict)
        self.assertIn("name", d)
        self.assertIn("sdesc", d)
        self.assertIn("popcon", d)
        self.assertIn("source", d)
        self.assertIn("ldesc", d)
        self.assertNotIn("stable_tags", d)
        self.assertIsInstance(d["todolist"], list)

        d = p.to_dict(with_todolist=False)
        self.assertIsInstance(d, dict)
        self.assertIn("name", d)
        self.assertIn("sdesc", d)
        self.assertIn("popcon", d)
        self.assertIn("source", d)
        self.assertIn("ldesc", d)
        self.assertIsInstance(d["stable_tags"], list)
        self.assertNotIn("todolist", d)

    def test_facet(self):
        "Test Facet core functions"
        p = bmodels.Facet.objects.get(name="role")

        # Test by_name
        p1 = bmodels.Facet.by_name("role")
        self.assertEqual(p, p1)
        self.assertEqual(bmodels.Facet.by_name("this-facet-does-not-exist"), None)

        # Test comparison with deferreds
        dp = bmodels.Facet.objects.filter(name="role").only("name")[0]
        self.assertEqual(p, dp)

        # Test members
        self.assertEqual(p.name, "role")
        self.assertEqual(p.sdesc, "Role")
        self.assertIn("performed", p.ldesc)
        self.assertIn("role::program", [t.name for t in p.tags.all()])

    def test_tag(self):
        "Test Tag core functions"
        p = bmodels.Tag.objects.get(name="role::program")

        # Test by_name
        p1 = bmodels.Tag.by_name("role::program")
        self.assertEqual(p, p1)
        self.assertEqual(bmodels.Tag.by_name("this-tag-does-not-exist"), None)

        # Test comparison with deferreds
        dp = bmodels.Tag.objects.filter(name="role::program").only("name")[0]
        self.assertEqual(p, dp)

        # Test members
        self.assertEqual(p.name, "role::program")
        self.assertEqual(p.tag_name, "program")
        self.assertEqual(p.sdesc, "Program")
        self.assertIn("computer", p.ldesc)
        self.assertEqual("role", p.facet.name)

    def test_maintainer(self):
        "Test Maintainer core functions"
        p = bmodels.Maintainer.objects.get(email="enrico@debian.org")

        # Test by_email
        p1 = bmodels.Maintainer.by_email("enrico@debian.org")
        self.assertEqual(p, p1)
        self.assertEqual(bmodels.Maintainer.by_email("this-email-does-not-exist"), None)

        # Test comparison with deferreds
        dp = bmodels.Maintainer.objects.filter(email="enrico@debian.org").only("name")[0]
        self.assertEqual(p, dp)

        # Test comparison
        p1 = bmodels.Maintainer(name="Enrico Zini", email="enrico@enricozini.org")
        self.assertNotEqual(p, p1)

        # Test members
        self.assertEqual(p.name, "Enrico Zini")
        self.assertEqual(p.email, "enrico@debian.org")
        self.assertEqual(set(x.name for x in p.packages.all()), set(("debtags", "goplay", "libept1", "libept-dev", "python-debian")))

    def test_patch(self):
        from debdata import patches

        db = bmodels.Patches()

        pkg = bmodels.Package.by_name("debtags")
        self.assertNotIn("special::not-yet-tagged", [x.name for x in pkg.stable_tags.all()])

        # Apply a patch
        patchset = patches.PatchSet()
        patchset.add("debtags", set(("admin::accounting",)), set())
        res, checker = db.apply_user_patchset(self.user, patchset)

        # Check the effects
        self.assertEqual(checker.remarks, [])
        self.assertEqual([r.name for r in res], ["debtags"])
        pkg = bmodels.Package.by_name("debtags")
        self.assertIn("admin::accounting", [x.name for x in pkg.stable_tags.all()])
        audit = bmodels.AuditLog.objects.get(user=self.user)
        self.assertEquals(audit.text, "debtags: +admin::accounting")

    def test_tag_debtags_db(self):
        db = bmodels.tag_debtags_db()
        self.assertTrue(db.has_package("debtags"))
        self.assertTrue(db.has_tag("role::program"))
        self.assertIn("role::program", db.tags_of_package("debtags"))
        self.assertIn("debtags", db.packages_of_tag("role::program"))

#    def test_update(self):
#        # Test that updates are idempotent
#        updater = self.build_test_db()
#        self.assertEqual(updater.count_packages_created, 0)
#        self.assertEqual(updater.count_packages_updated, 0)
#        self.assertEqual(updater.count_packages_deleted, 0)
#        self.assertEqual(updater.count_facets_created, 0)
#        self.assertEqual(updater.count_facets_updated, 0)
#        self.assertEqual(updater.count_facets_deleted, 0)
#        self.assertEqual(updater.count_tags_created, 0)
#        self.assertEqual(updater.count_tags_updated, 0)
#        self.assertEqual(updater.count_tags_deleted, 0)
#        self.assertEqual(updater.count_stable_tags_added, 0)
#        self.assertEqual(updater.count_stable_tags_removed, 0)
#        self.assertEqual(updater.count_unstable_tags_added, 0)
#        self.assertEqual(updater.count_unstable_tags_removed, 0)
#        self.assertEqual(updater.count_checks_created, updater.count_checks_removed)
#        self.assertEqual(updater.count_nyt_tags_added, 0)
#        self.assertEqual(updater.count_autodebtag_patches, 0)
