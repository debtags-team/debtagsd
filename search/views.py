from __future__ import annotations
from django import forms
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from debtagslayout.mixins import DebtagsLayoutMixin
from api import models as amodels


class SearchForm(forms.Form):
    q = forms.CharField(required=False, label=_("Search"))
    qf = forms.ChoiceField(required=False, label=_("Filter"), choices=[(x, y["desc"]) for x, y in amodels.FILTERS])
    wl = forms.CharField(required=False, widget=forms.HiddenInput())


class Search(DebtagsLayoutMixin, TemplateView):
    template_name = "search/search.html"

    def get_context_data(self, **kw):
        ctx = super(Search, self).get_context_data(**kw)

        query = self.request.GET.get("q", None)
        res = None
        facets = None
        facets_sel = set()
        if query is None:
            form = SearchForm()
        else:
            form = SearchForm(self.request.GET)
            if form.is_valid():
                wl = set(x for x in form.cleaned_data["wl"].split(",") if x)
                res = amodels.search_query(form.cleaned_data["q"], form.cleaned_data["qf"], whitelist=wl, count_tags=20)

                # Group resulting tags by facet
                by_facet = dict()
                for t in res["tags"]:
                    by_facet.setdefault(t["tag"].facet, []).append(t)

                # Sort by short descriptions
                facets = []
                facets_rescount = []
                for facet, tags in by_facet.items():
                    has_sel = any(t["sel"] for t in tags)
                    # Take note of which facets are selected
                    if has_sel:
                        facets_sel.add(facet.name)
                    # Remove tags with too small a package number
                    tags = [t for t in tags if t["sel"] or t["w"] > 1]
                    # Skip facets with only one tag and no currently selected tags
                    if len(tags) < 2 and not has_sel:
                        continue
                    # Sort tags by short description
                    tags = sorted(tags, key=lambda x: x["tag"].sdesc)
                    facets.append((facet, tags))
                    facets_rescount.append((sum(t["w"] for t in tags), facet.name))

                # Keep only the top-5 facets with the highest number of results
                if len(facets) > 5:
                    facets_rescount.sort(reverse=True)
                    top_facets = set(x[1] for x in facets_rescount[:5])
                    facets = [(facet, tags) for facet, tags in facets
                              if facet.name in facets_sel or facet.name in top_facets]

                facets.sort(key=lambda x: x[0].sdesc)

        ctx.update(
            form=form,
            res=res,
            facets=facets,
            has_sel=(len(facets_sel) > 0),
        )
        return ctx
