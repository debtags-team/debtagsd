from __future__ import annotations
from django.utils.module_loading import import_string
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from signon import providers

_name = getattr(settings, "SIGNON_TEST_FIXTURE", None)
if _name is None:
    raise ImproperlyConfigured(
            "You need to set SIGNON_TEST_FIXTURE to a valid dotted module.class path, to use signon unittests")


class SignonFixtureMixin(import_string(_name)):
    def setUp(self):
        super().setUp()
        # Reset the providers cache before each test
        providers.providers_by_name = None


__all__ = ["SignonFixtureMixin"]
