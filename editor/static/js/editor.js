/*
 * debtagsd editor interface
 *
 * Copyright (C) 2011  Enrico Zini <enrico@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Facet and tag information
var voc = new Vocabulary(fdata, tdata);

// Currently selected facet in the all tags view
var atcurfacet = "role";

// List of suggested tags for this package
var suggested = [];

// Results of the tag search
var relevant = [];

// URL used to submit patches
var search_tags_url = "https://debtags.debian.org/tag/search";

function curtag_tooltip_handler()
{
    var tag = $(this).attr("tag");
    var finfo = voc.facData(voc.facetOfTag(tag));
    var tinfo = voc.tagData(tag);
    var res = "<div class='tooltip'>"
    res += "Facet: <b>" + finfo[0] + "</b>";
    if (finfo[1])
        res += "<p>"+finfo[1]+"</p>";
    res += "Tag: <b>" + tinfo[0] + "</b>";
    if (tinfo[1])
        res += "<p>"+tinfo[1]+"</p>";
    res += "</div>";
    return res;
}
function curtag_hooks(el)
{
    el.tooltip({
        bodyHandler: curtag_tooltip_handler,
    })
    return el;
}

function alltags_facet_selected()
{
    var el = $(this);
    var facet = el.attr("facet");
    atcurfacet = facet;
    update_available();
}

// Update visibility in the list of available tags
function update_available()
{
    var at_facs = $("#available_facets_list");
    var at_tags = $("#available_tags_list");
    var tagfilter = document.alltags.tagsearch.value.toLowerCase();
    var facfilter = atcurfacet + "::";

    // Mark the current facet as current
    $("#available_facets_list li.current").removeClass("current");
    $("#available_facets_list li[facet=\""+atcurfacet+"\"]").addClass("current");

    // View the name of the currently selected facet
    var facDesc = voc.facData(atcurfacet);
    $("#available_selected_facet").text(facDesc[0]);

    // Update tag visibility
    var facet_visible = {};
    $(".tag", at_tags).each(function(idx, el) {
        var el = $(el);
        var tag = el.attr("tag");
        var taginfo = tdata[tag];
        var visible = true;

        if (tagfilter)
        {
            var text = (tag + " " + taginfo[0] + " " + taginfo[1]).toLowerCase();
            if (text.indexOf(tagfilter) == -1)
                visible = false;
        }

        if (visible)
            // Mark this facet as visible
            facet_visible[tag.split("::")[0]] = true;

        if (visible && tag.slice(0, facfilter.length) != facfilter)
            visible = false;

        if (visible)
            el.parent().show();
        else
            el.parent().hide();
    });

    $("li", at_facs).each(function(idx, el) {
        var el = $(el);
        var facet = el.attr("facet");
        if (facet_visible[facet])
            el.show();
        else
            el.hide();
    });
}

function show_random_tip()
{
    var idx = Math.round(Math.random() * tips.length);
    while (idx == tips.length)
        idx = Math.round(Math.random() * tips.length);
    var tipbox = $("#tip");
    tipbox.empty();
    tipbox.append(tips[idx]);
}

function search_tags(query)
{
    if (query == null)
        query = $("#tag_search_query").val();

    var search_tags_list = $("#search_tags_list");
    search_tags_list.empty();

    document.relsearch.keys.disabled = true;
    document.relsearch.submit.disabled = true;

    $.getJSON(search_tags_url + encodeURIComponent(query), null,
        function(data, textStatus, jqXHR) {
            document.relsearch.keys.disabled = false;
            document.relsearch.submit.disabled = false;
            $.each(data.tags, function(idx, el) {
                var tag_el = render_tag(el.n);
                tag_el.tooltip({
                    bodyHandler: curtag_tooltip_handler,
                })
                search_tags_list.append($("<li>").append(tag_el));
            });
        })
        .error(function(jqXHR, textStatus, errorThrown) {
            alert("Tag search failed: " + textStatus + " " + errorThrown);
            document.relsearch.keys.disabled = false;
            document.relsearch.submit.disabled = false;
        });
}

// From http://www.quirksmode.org/js/cookies.html
function readCookie(name)
{
   var nameEQ = name + "=";
   var ca = document.cookie.split(';');
   for (var i=0; i < ca.length; ++i)
   {
       var c = ca[i];
       while (c.charAt(0)==' ') c = c.substring(1, c.length);
       if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
   }
   return null;
}

// TODO: besides the hasCookies check, generate the rest from the checks module

var hasCookies = readCookie("patchtag") != null;

function commentTagset(pkg, tags)
{
    var res = check_tags(tags);
    return res;
}


(function( $ ){

// Patch editor
class PatchEditor
{
    constructor(element, options)
    {
        this.element = $(element);

        this.pkg = options.pkg;

        // Patch that we interact with
        this.current_patch = options.patch;

        // Url to which the patchset is submitted
        this.post_url = options.post_url;

        // Token used for csrf protection
        this.csrf_token = options.csrf_token;

        if (this.current_patch == null)
            throw("patch must be defined");
        if (this.post_url == null)
            throw("post_url must be defined");

        // Locate UI elements
        this._submit = this.element.find("div.dui-patchset-submit");
        let group = this._submit.find("span.dui-patchset-submit-label");

        this._submit_summary = group.find("span.dui-patchset-submit-summary");
        this._submit_spinner = group.find("span.dui-spinner");

        this._submit_button = this._submit.find("button.dui-patchset-submit-button");
        this._submit_button.bind("click", () => { this.submit().then(); });

        this._submit_messages = this.element.find("div.dui-patchset-submit-messages");
        this._submit_messages_list = this._submit_messages.find("ul");
        this._submit_messages.hide();
        this._submit_messages.bind("click", () => { this._submit_messages.hide(); });

        this._patch = this.element.find("ul.dui-patchset-patch");

        this.reset_tags(options.initial_tags);

        this.refresh();
    }

    refresh()
    {
        var self = this;
        let patch = this.current_patch;

        this._patch.empty();

        patch.each(p => {
            const tag = p.substr(1);
            var li = $("<li>").addClass("dui-tagname").attr("tag", tag).text(p).bind("click.patchset", (evt) => {
                this.patch_reset(tag);
            });
            this._patch.append(li);
        });

        var count_changes = this.current_patch.count_changes();
        if (count_changes > 0)
        {
            if (count_changes == 1)
                self._submit_summary.text("There is one change to submit:");
            else
                self._submit_summary.text("There are " + count_changes + " changes to submit:");
            self._submit_button.attr("disabled", false);
            self.element.removeClass("dui-empty");
        } else {
            self._submit_summary.text("There are no changes to submit.");
            self._submit_button.attr("disabled", true);
            self.element.addClass("dui-empty");
        }
    }

    patch(added, removed)
    {
        var touched = this.current_patch.update(added, removed);
        this.tags.patch(added, removed);
        this.current_patch.simplify(this.orig_tags);
        this.refresh();
        this.update_tips();
        this.update_current_tag_list();
        if (touched.length > 0)
            this.element.trigger("changed", { pkg: pkg, tags: touched });
        this._submit_messages.hide();
    }

    patch_reset(tag)
    {
        if (this.current_patch.is_added(tag))
            this.patch([], [tag]);
        else if (this.current_patch.is_removed(tag))
            this.patch([tag], []);
    }

    async submit() {
        var patch = this.current_patch.to_string();
        // If the patch is empty, do nothing
        if (!patch) return;
        var data = {
            "patch": this.pkg + ": " + patch,
            "csrfmiddlewaretoken": this.csrf_token,
        };

        this.element.trigger("presubmit", data);

        this._submit_messages_list.empty();
        this._submit_messages.hide();
        this._submit_spinner.removeClass("dui-idle");
        this._submit_button.attr("disabled", true);
        let res = await ajax({
            "url": this.post_url,
            "method": "POST",
            "data": data,
        });
        this._submit_button.attr("disabled", false);
        this._submit_spinner.addClass("dui-idle");

        // show remarks
        if (res.notes.length > 0)
        {
            for (const note of res.notes)
                this._submit_messages_list.append($("<li>").append(note));
            this._submit_messages.show();
        }
        this.current_patch.clear();
        this.refresh();
        this.reset_tags(res.pkgs[this.pkg]);

        /* TODO: on reject of the ajax future:
            .error(function(jqXHR, textStatus, errorThrown) {
                self._submit_button.attr("disabled", false);
                self._submit_spinner.addClass("dui-idle");
                self.element.trigger("postsubmit", null, { success: false, data: errorThrown, textStatus: textStatus });
            });
        */
    }

    // Render a tag with the given name, returning the DOM object for it
    render_tag(tname)
    {
        var tinfo = tdata[tname];
        if (tinfo == null)
        {
            tinfo = ["(no description)", null];
        }
        var span = $("<span>").addClass("tag").addClass("menu").attr("tag", tname)
            .append($("<span>").addClass("name").text(tname))
            .append(": ")
            .append($("<span>").addClass("desc").append(tinfo[0]));
        if (tinfo[1])
            span.append($("<div>").addClass("ldesc").append(tinfo[1]));

        if (this.tags.has(tname))
            span.addClass("current");

        span.click(evt => {
            if (this.tags.has(tname))
                this.del_tag(tname);
            else
                this.add_tag(tname);
        });

        return span;
    }

    // Add a tag to the package
    add_tag(name)
    {
        this.patch([name], []);
    }

    // Remove a tag from the package
    del_tag(name)
    {
        this.patch([], [name]);
    }

    // Render a tag for the current tag list
    render_selected_tag(tname)
    {
        var span = $("<span>").addClass("tag").attr("tag", tname)
            .append($("<span>").addClass("name").text(tname));
        span.click((evt) => { this.del_tag(tname); });
        return span;
    }

    update_current_tag_list()
    {
        var tags = this.tags.to_list();
        tags.sort();

        // Render the list of current tags
        var current_tag_list = $("#current_tag_list");
        current_tag_list.empty();
        let count = 0;
        for (const tag of tags)
        {
            current_tag_list.append($("<li>").append(curtag_hooks(this.render_selected_tag(tag))));
            count += 1;
        }
        if (count == 0)
            current_tag_list.append($("<li>").addClass("no_tags").append("None"));
    }

    reset_tags(new_tags)
    {
        this.orig_tags = new Tagset();
        this.orig_tags.add_all(new_tags);

        this.tags = new Tagset();
        this.tags.add_all(new_tags);

        this.update_current_tag_list();

        // Update the visibility of all tags
        $(".menu.tag").each((idx, el) => {
            var el = $(el);
            var tag = el.attr("tag");
            if (this.tags.has(tag))
                el.addClass("current");
            else
                el.removeClass("current");
        });
        this.update_tips();
    }

    update_tips()
    {
        var comments = commentTagset(this.pkg, this.tags);
        var todolist = $("#todolist");
        var todolist_list = $("ul", todolist);
        var has_any = false;

        todolist_list.empty();
        if (comments.length > 0)
        {
            for (let c of comments)
            {
                var entry = $("<li>").addClass(c.type).append(c.msg);
                if (c.fixes.length > 0)
                {
                    var fixlist = $("<ul>").addClass("fixes");
                    for (let f of c.fixes) {
                        var fixentry = $("<li>").html(f.msg);
                        fixlist.append(fixentry);
                        fixentry.click(evt => {
                            $.each(f.patch.split(", "), (idx, el) => {
                                if (el[0] == "+") {
                                    this.add_tag(el.substr(1));
                                } else if (el[0] == "-") {
                                    this.del_tag(el.substr(1));
                                }
                            });
                        });
                    }
                    entry.append(fixlist);
                }
                todolist_list.append(entry);
            }
            has_any = true
        }

        if (has_any)
            todolist.show();
        else
            todolist.hide();

    }
}

window.debtags = $.extend(window.debtags || {}, {
    PatchEditor: PatchEditor,
});

})(jQuery);
