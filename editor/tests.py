from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import BackendDataMixin


@override_settings(BACKEND_DATADIR="testdata")
class TestView(BackendDataMixin, TestCase):
    def test_index(self):
        client = self.make_test_client()
        response = client.get(reverse("editor_index"))
        self.assertRedirects(response, reverse("root_doc_sso"))

        response = client.get(reverse("editor_index") + "?pkg=debtags")
        self.assertRedirects(response, reverse("root_doc_sso"))

        client = self.make_test_client(self.user)
        response = client.get(reverse("editor_index"))
        self.assertContains(response, "enter a package name to edit its tags")

        response = client.get(reverse("editor_index") + "?pkg=debtags")
        self.assertRedirects(response, reverse("editor_edit", args=["debtags"]))

    def test_edit(self):
        client = self.make_test_client()
        response = client.get(reverse("editor_edit", args=["debtags"]))
        self.assertRedirects(response, reverse("root_doc_sso"))

        client = self.make_test_client(self.user)
        response = client.get(reverse("editor_edit", args=["debtags"]))
        self.assertContains(response, "Checks and hints")
