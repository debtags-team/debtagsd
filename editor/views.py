from __future__ import annotations
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django import http
from django.shortcuts import redirect
import json
from debtagslayout.views import DebtagsTemplateView
from backend import models as bmodels
from aptxapianindex.axi import AptXapianIndex


class Index(DebtagsTemplateView):
    template_name = "editor/index.html"

    def get(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            return redirect("root_doc_sso")
        name = request.GET.get("pkg", "").strip()
        if not name:
            return super().get(request, *args, **kw)
        else:
            return redirect("editor_edit", name=name)


class Edit(DebtagsTemplateView):
    template_name = "editor/edit.html"

    def dispatch(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            return redirect("root_doc_sso")
        return super().dispatch(request, *args, **kw)

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        name = self.kwargs["name"]
        pkg = bmodels.Package.by_name(name)
        if pkg is None:
            raise http.Http404(_("Package {} was not found").format(name))
        if pkg.trivial:
            raise PermissionDenied(_("trivial packages cannot be edited"))
        package = pkg

        # Compute tag suggestions
        axi = AptXapianIndex()
        suggested_tags = json.dumps(list(axi.suggest_tags(package, count=15)))

        ctx.update(
            pkg=package,
            tags=json.dumps([x.name for x in package.stable_tags.all()]),
            suggested_tags=suggested_tags,
        )

        return ctx
