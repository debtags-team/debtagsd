# coding: utf8
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
import django_housekeeping as hk
from debdata.utils import atomic_writer
import os
import backend.models as bmodels
import logging

log = logging.getLogger(__name__)


STAGES = ["main", "exports"]


class ExportJS(hk.Task):
    """
    Export tags
    """
    def run_exports(self, stage):
        from django.template.loader import get_template
        import json

        # Export the tag vocabulary in javascript
        tpl = get_template("exports/vocabulary.js")
        facets = dict((f.name, (f.sdesc, f.ldesc_html)) for f in bmodels.Facet.objects.all())
        tags = dict((t.name, (t.sdesc, t.ldesc_html)) for t in bmodels.Tag.objects.all())
        res = tpl.render({
            "facets": json.dumps(facets, separators=(',', ':')),
            "tags": json.dumps(tags, separators=(',', ':')),
        })

        if os.path.isdir("../htdocs/js"):
            pathname = os.path.abspath("../htdocs/js/vocabulary.js")
        else:
            pathname = os.path.abspath("debtagslayout/static/js/vocabulary.js")

        with atomic_writer(pathname, "wt") as fd:
            fd.write(res)
