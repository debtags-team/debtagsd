from __future__ import annotations
from django.conf.urls import url
from debtagslayout.views import DebtagsTemplateView
from . import views

urlpatterns = [
    url(r'^$', DebtagsTemplateView.as_view(template_name='exports/index.html'), name="exports_index"),
    url(r'^stable-tags$', views.stable_tags_view, name="exports_stable_tags"),
    url(r'^vocabulary.js$', views.vocabulary_js_view, name="exports_vocabulary_js"),
    url(r'^tagchecks.js$', views.tagchecks_js, name="exports_tagchecks_js"),
    url(r'^overrides/(?P<name>[a-z-]+)$', views.Overrides.as_view(), name="exports_overrides"),
]
