# coding: utf8
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.db import models
from . import checks
# Included but not used, but it's a way to have checks registered in the
# system.

# Create your models here.
