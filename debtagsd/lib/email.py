import time
import email.message
import email.utils
from email.header import Header
from email.headerregistry import parser, Address
from email.utils import formataddr

from django.core.mail import EmailMessage
from django.utils.timezone import now


def _to_header(values):
    from backend.models import Person
    addrs = []
    if not isinstance(values, list):
        values = [values]

    for value in values:
        if isinstance(value, str):
            addrs.append(('', value))
        elif isinstance(value, Person):
            addrs.append((value.fullname, value.email))
        else:
            addrs.append(value)

    return ", ".join(email.utils.formataddr(a) for a in addrs)


def build_python_message(
        from_email=None, to=None, cc=None, reply_to=None, subject=None,
        date=None, body="", factory=email.message.Message):
    """
    Build an email.message.Message, or equivalent object, from common
    arguments.

    If from_email, to, cc, bcc, reply_to can be strings or bmodels.Person objects.
    """
    # Generating mailboxes in python2 is surprisingly difficult and painful.
    # A lot of this code has been put together thanks to:
    # http://wordeology.com/computer/how-to-send-good-unicode-email-with-python.html

    if from_email is None:
        from_email = "nm@debian.org"
    if subject is None:
        subject = "Notification from nm.debian.org"
    if date is None:
        date = now()

    msg = factory()
    msg["From"] = _to_header(from_email)
    msg["Subject"] = Header(subject, "utf-8")
    msg["Date"] = email.utils.formatdate(time.mktime(date.timetuple()))
    if to:
        msg["To"] = _to_header(to)
    if cc:
        msg["Cc"] = _to_header(cc)
    if reply_to:
        msg["Reply-To"] = _to_header(reply_to)
    msg.set_payload(body, "utf-8")
    return msg


def _clean_element(value):
    """Cleans properly names or localpart of an email address"""

    return Header(value, maxlinelen=1000).encode()


def _clean_email_addr(local, domain):
    """Takes local part and domain part and cleans them properly to return a
    correctly built email address"""

    local = _clean_element(local)
    domain = domain.encode('idna').decode()

    return Address(username=local, domain=domain).addr_spec


def _str_to_addr(value):
    """Transforms a string into a properly formatted email address"""

    if '\n' in value or '\r' in value:
        raise ValueError("%s is not a proper mail string" % (value,))

    content, remains = parser.get_mailbox(value)
    if remains:
        raise ValueError("%s is not a proper mail string" % (value,))
    name, local, domain = (
        content.display_name,
        content.local_part,
        content.domain or '',
    )

    return _tuple_to_addr((name, f'{local}@{domain}'))


def _tuple_to_addr(value):
    """Transforms a (name, email) tuple into a properly formatted email address
    header"""

    return formataddr(
        (
            _clean_element(value[0]),
            _clean_email_addr(*(value[1].rsplit('@', 1))),
        )
    )


def _to_django_addr(value):
    from backend.models import Person
    if isinstance(value, str):
        return _str_to_addr(value)
    elif isinstance(value, tuple):
        return _tuple_to_addr(value)
    elif isinstance(value, Person):
        return _tuple_to_addr((value.fullname, value.email))
    else:
        raise TypeError("argument is not a string or Person")


def _to_django_addrlist(value):
    if isinstance(value, list):
        return [_to_django_addr(x) for x in value]
    else:
        return [_to_django_addr(value)]


def build_django_message(
        from_email=None, to=None, cc=None, bcc=None, reply_to=None, subject=None, date=None, headers=None, body=""):
    """
    Build a Django EmailMessage from common arguments.

    If from_email, to, cc, bcc, reply_to can be strings or bmodels.Person objects.
    """
    # Generating mailboxes in python2 is surprisingly difficult and painful.
    # A lot of this code has been put together thanks to:
    # http://wordeology.com/computer/how-to-send-good-unicode-email-with-python.html

    if from_email is None:
        from_email = "nm@debian.org"
    if subject is None:
        subject = "Notification from nm.debian.org"
    if date is None:
        date = now()

    kw = {}
    if to is not None:
        kw["to"] = _to_django_addrlist(to)
    if cc is not None:
        kw["cc"] = _to_django_addrlist(cc)
    if bcc is not None:
        kw["bcc"] = _to_django_addrlist(bcc)
    if reply_to is not None:
        kw["reply_to"] = _to_django_addrlist(reply_to)
    if headers is None:
        headers = {}
    headers.update(Date=email.utils.formatdate(time.mktime(date.timetuple())))

    msg = EmailMessage(
        from_email=_to_django_addr(from_email),
        subject=subject,
        body=body,
        headers=headers,
        **kw
    )
    return msg
