from __future__ import annotations
from django.urls import path
from debtagslayout.views import DebtagsTemplateView
from . import views

app_name = "user"

urlpatterns = [
    path(r'profile/', views.Profile.as_view(), name="profile"),
]
