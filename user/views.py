from __future__ import annotations
from django.core.exceptions import PermissionDenied
from django.views.generic.edit import UpdateView
from django.urls import reverse
from debtagsd.lib.forms import BootstrapAttrsMixin
from debtagslayout.mixins import DebtagsLayoutMixin
from backend.models import User


class Profile(DebtagsLayoutMixin, UpdateView):
    model = User
    fields = "maintainer_email",
    template_name = "user/profile.html"

    def get_object(self):
        return self.request.user

    def get_form_class(self):
        # Add BootstrapAttrsMixin to the standard UpdateView form
        cls = super().get_form_class()
        return type("Form", (BootstrapAttrsMixin, cls), {})

    def dispatch(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            raise PermissionDenied
        return super().dispatch(request, *args, **kw)

    def get_success_url(self):
        return reverse("user:profile")
